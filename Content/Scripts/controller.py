import sys

import unreal_engine as ue
import numpy as np
import cv2

ue.log("Python version: ".format(sys.version))

class PythonAIController(object):
    # Called at the started of the game
    def begin_play(self):
        ue.log("Begin Play on PythonAIController class")

    def get_screen(self, game_mode):
        if not game_mode:
            return None

        screen_capturer = game_mode.ScreenCapturer
        screenshot = np.array(screen_capturer.Screenshot)
        H = screen_capturer.Height
        W = screen_capturer.Width

        if len(screenshot) == 0:
            return None

        return screenshot.reshape((H, W, 3), order='F').swapaxes(0, 1)[:,:,::-1]
    
    # Called periodically during the game
    def tick(self, delta_seconds : float):
        ue.log("Tick on PythonAIController class")
        pawn = self.uobject
        up = pawn.UpPressed
        down = pawn.DownPressed
        right = pawn.RightPressed
        left = pawn.LeftPressed
        screen = self.get_screen(pawn.GameModeRef)
        if not screen is None:
            ue.log("Screen shape: {}".format(screen.shape))
            cv2.imwrite("C:/Users/Ilya/Documents/Unreal Projects/PacMan/tmp/screen.png", 255.0 * screen)
        else:
            ue.log("Screen is not available")